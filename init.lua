doors.register_door("seinfeld_doors:door", {
    description = "Seinfeld Door",
    inventory_image = "doors_item_wood.png",
    groups = {choppy=2, cracky=2, door=1},
    tiles = {{name="doors_door_wood.png", backface_culling = true }},
    protected = false,
    sound_open = "seinfeld_doors_door_open"
})
